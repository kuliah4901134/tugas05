package id.ac.ub.room;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    Button bt1;
    Button bt2;
    EditText et1;
    RecyclerView recyclerView;
    private AppDatabase appDb;

    ArrayList<Item> testing = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appDb = AppDatabase.getInstance(getApplicationContext());
        bt1 = findViewById(R.id.bt1);
        bt2 = findViewById(R.id.bt2);
        recyclerView = findViewById(R.id.recycleview);
        et1 = findViewById(R.id.et1);

        ArrayList<Item> item = new ArrayList<>();
        ItemListAdapter itemListAdapter = new ItemListAdapter(this, item);
        recyclerView.setAdapter(itemListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Item test = new Item();
                test.judul = et1.getText().toString();
                appDb.itemDao().insertAll(test);
            }
        });
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nampilin(testing);
            }
        });
    }

    void nampilin(ArrayList<Item> list){
        ArrayList a = new ArrayList<>(appDb.itemDao().getAll());
        ItemListAdapter p = new ItemListAdapter(this, a);
        recyclerView.setAdapter(p);
    }
}